#cd infrastructure/proto
#protoc --proto_path=. --go_out=paths=source_relative:. infrastructure/proto/org_api/enums/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/org_api/user/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/org_api/basic/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/org_api/finance/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/org_api/payment/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/org_api/account/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/org_api/statistics/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/user_info/user_info/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/partner_api/partner/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/partner_api/invite/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/partner_api/notice/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/partner_api/account/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/partner_api/position/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/partner_api/report/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/finance_api/base/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/finance_api/single_lock/*.proto
protoc --go_out=paths=source_relative:.  infrastructure/proto/quotation/tick/*.proto
protoc --go_out=paths=source_relative:.  infrastructure/proto/quotation/kline/*.proto
protoc --go_out=paths=source_relative:.  infrastructure/proto/quotation/leverage/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:. infrastructure/proto/finance_api/transfer/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:. infrastructure/proto/message_api/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/message/aliyun_push/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/oss_api/*.proto
protoc --go_out=plugins=grpc,paths=source_relative:.  infrastructure/proto/rollingdata_api/register/*.proto

package user_info_client

import (
	"fmt"
	userInfo "gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/user_info/user_info"
	"google.golang.org/grpc"
	"sync"
	"sync/atomic"
	"unsafe"
)

type UserInfoClient struct {
	globalClientConn        unsafe.Pointer
	lck                     sync.Mutex
	targetConnectionAddress string
}

var UserInfoClientInstance = &UserInfoClient{}

func (o *UserInfoClient) Init(host, port string) {
	o.targetConnectionAddress = fmt.Sprintf("%v:%v", host, port)
}

func (o *UserInfoClient) GetUserInfoClient() (userInfo.GetUserInfoClient, error) { //返回需要的client
	conn, err := o.getConn(o.targetConnectionAddress)
	if err != nil {
		return (userInfo.GetUserInfoClient)(nil), err
	}
	return userInfo.NewGetUserInfoClient(conn), nil //此处调用pb.go文件中生成的创建client的方法
}

func (o *UserInfoClient) getConn(target string) (*grpc.ClientConn, error) {
	if atomic.LoadPointer(&o.globalClientConn) != nil {
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	o.lck.Lock()
	defer o.lck.Unlock()
	if atomic.LoadPointer(&o.globalClientConn) != nil { //double check
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	cli, err := o.newGrpcConn(target)
	if err != nil {
		return nil, err
	}
	atomic.StorePointer(&o.globalClientConn, unsafe.Pointer(cli))
	return cli, nil
}

func (o *UserInfoClient) newGrpcConn(target string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(
		target,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

package org_api_client

import (
	"context"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/user"
	"testing"
)

const authToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MDYyMjQ1Mzc5Mzk2MzIxMjgiLCJfdGltZSI6MTU5NTg0MDQ5MDY2OCwiX3IiOiJLcDVSVEJFVXBXWmoiLCJfcCI6Ijg0ODIxYzBmZTMxN2MyOTM4ZWY5ZGFmZmFmZDQyMmNlIn0.n8jOtlqrXsCI4D9Dum-F5ZrVVRCr_yqMhDK32y6iZZo"

func TestOrgApiClient_GetUserClient(t *testing.T) {
	OrgApiClientInstance.Init("internal-a9e064b93c98e4720932a3d3d868448e-2048774476.ap-northeast-1.elb.amazonaws.com", "80")
	client, err := OrgApiClientInstance.GetUserClient()
	if err == nil {
		ctx := context.Background()
		result, err := client.ParseToken(ctx, &user.ParseTokenReq{
			Token:     authToken,
			TokenFrom: "",
		})
		if err == nil {
			t.Log(result)
		} else {
			t.Error(err)
		}
	}
}

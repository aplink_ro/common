package org_api_client

import (
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/account"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/basic"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/finance"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/payment"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/statistics"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/org_api/user"
	"google.golang.org/grpc"
	"sync"
	"sync/atomic"
	"unsafe"
)

type OrgApiClient struct {
	globalClientConn        unsafe.Pointer
	lck                     sync.Mutex
	targetConnectionAddress string
	userClient              user.UserClient
	financeClient           finance.FinanceClient
	paymentClient           payment.PaymentClient
	statisticsClient        statistics.StatisticsClient
	accountClient           account.AccountClient
	basicClient             basic.BasicClient
}

var OrgApiClientInstance = &OrgApiClient{}

func (o *OrgApiClient) Init(host, port string) {
	o.targetConnectionAddress = fmt.Sprintf("%v:%v", host, port)
}

func (o *OrgApiClient) GetUserClient() (user.UserClient, error) {
	if o.userClient == nil {
		o.lck.Lock()
		defer o.lck.Unlock()
		conn, err := o.getConn(o.targetConnectionAddress)
		if err != nil {
			return (user.UserClient)(nil), err
		}
		o.userClient = user.NewUserClient(conn)
	}
	return o.userClient, nil
}

func (o *OrgApiClient) GetFinanceClient() (finance.FinanceClient, error) {
	if o.financeClient == nil {
		o.lck.Lock()
		defer o.lck.Unlock()
		conn, err := o.getConn(o.targetConnectionAddress)
		if err != nil {
			return (finance.FinanceClient)(nil), err
		}
		o.financeClient = finance.NewFinanceClient(conn)
	}
	return o.financeClient, nil

}

func (o *OrgApiClient) GetPaymentClient() (payment.PaymentClient, error) {
	if o.paymentClient == nil {
		o.lck.Lock()
		defer o.lck.Unlock()
		conn, err := o.getConn(o.targetConnectionAddress)
		if err != nil {
			return (payment.PaymentClient)(nil), err
		}
		o.paymentClient = payment.NewPaymentClient(conn)
	}
	return o.paymentClient, nil
}

func (o *OrgApiClient) GetStatisticsClient() (statistics.StatisticsClient, error) {
	if o.statisticsClient == nil {
		o.lck.Lock()
		defer o.lck.Unlock()
		conn, err := o.getConn(o.targetConnectionAddress)
		if err != nil {
			return (statistics.StatisticsClient)(nil), err
		}
		o.statisticsClient = statistics.NewStatisticsClient(conn)
	}
	return o.statisticsClient, nil
}

func (o *OrgApiClient) GetBasicClient() (basic.BasicClient, error) {
	if o.basicClient == nil {
		o.lck.Lock()
		defer o.lck.Unlock()
		conn, err := o.getConn(o.targetConnectionAddress)
		if err != nil {
			return (basic.BasicClient)(nil), err
		}
		o.basicClient = basic.NewBasicClient(conn)
	}
	return o.basicClient, nil
}

func (o *OrgApiClient) GetAccountClient() (account.AccountClient, error) {
	if o.accountClient == nil {
		o.lck.Lock()
		defer o.lck.Unlock()
		conn, err := o.getConn(o.targetConnectionAddress)
		if err != nil {
			return (account.AccountClient)(nil), err
		}
		o.accountClient = account.NewAccountClient(conn)
	}
	return o.accountClient, nil
}

func (o *OrgApiClient) getConn(target string) (*grpc.ClientConn, error) {
	if atomic.LoadPointer(&o.globalClientConn) != nil {
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	if atomic.LoadPointer(&o.globalClientConn) != nil { //double check
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	cli, err := o.newGrpcConn(target)
	if err != nil {
		return nil, err
	}
	atomic.StorePointer(&o.globalClientConn, unsafe.Pointer(cli))
	return cli, nil
}

func (o *OrgApiClient) newGrpcConn(target string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(
		target,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

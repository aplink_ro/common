package rolling_data_api_client

import (
	"fmt"
	rollingData "gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/rollingdata_api/register"
	"google.golang.org/grpc"
	"sync"
	"sync/atomic"
	"unsafe"
)

type RollingDataClient struct {
	globalClientConn        unsafe.Pointer
	lck                     sync.Mutex
	targetConnectionAddress string
}

var RollingDataClientInstance = &RollingDataClient{}

func (o *RollingDataClient) Init(host, port string) {
	o.targetConnectionAddress = fmt.Sprintf("%v:%v", host, port)
}

func (o *RollingDataClient) RollingDataClient() (rollingData.RollingDataClient, error) { //返回需要的client
	conn, err := o.getConn(o.targetConnectionAddress)
	if err != nil {
		return (rollingData.RollingDataClient)(nil), err
	}
	return rollingData.NewRollingDataClient(conn), nil //此处调用pb.go文件中生成的创建client的方法
}

func (o *RollingDataClient) getConn(target string) (*grpc.ClientConn, error) {
	if atomic.LoadPointer(&o.globalClientConn) != nil {
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	o.lck.Lock()
	defer o.lck.Unlock()
	if atomic.LoadPointer(&o.globalClientConn) != nil { //double check
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	cli, err := o.newGrpcConn(target)
	if err != nil {
		return nil, err
	}
	atomic.StorePointer(&o.globalClientConn, unsafe.Pointer(cli))
	return cli, nil
}

func (o *RollingDataClient) newGrpcConn(target string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(
		target,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

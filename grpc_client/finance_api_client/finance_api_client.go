package finance_api_client

import (
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/finance_api/transfer"
	"google.golang.org/grpc"
	"sync"
	"sync/atomic"
	"unsafe"
)

type FinanceApiClient struct {
	globalClientConn        unsafe.Pointer
	lck                     sync.Mutex
	targetConnectionAddress string
}

var FinanceApiClientInstance = &FinanceApiClient{}

func (o *FinanceApiClient) Init(host, port string) {
	o.targetConnectionAddress = fmt.Sprintf("%v:%v", host, port)
}

func (o *FinanceApiClient) GetFinanceApiClient() (transfer.FinanceApiTransferClient, error) { //返回需要的client
	conn, err := o.getConn(o.targetConnectionAddress)
	if err != nil {
		return (transfer.FinanceApiTransferClient)(nil), err
	}
	return transfer.NewFinanceApiTransferClient(conn), nil //此处调用pb.go文件中生成的创建client的方法
}

func (o *FinanceApiClient) getConn(target string) (*grpc.ClientConn, error) {
	if atomic.LoadPointer(&o.globalClientConn) != nil {
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	o.lck.Lock()
	defer o.lck.Unlock()
	if atomic.LoadPointer(&o.globalClientConn) != nil { //double check
		return (*grpc.ClientConn)(o.globalClientConn), nil
	}
	cli, err := o.newGrpcConn(target)
	if err != nil {
		return nil, err
	}
	atomic.StorePointer(&o.globalClientConn, unsafe.Pointer(cli))
	return cli, nil
}

func (o *FinanceApiClient) newGrpcConn(target string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(
		target,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

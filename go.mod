module gitlab.com/mini-tech/aptoken/aplink-common

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	gitlab.yuntron.org/backend/core v0.0.0-20210629021439-623f14a150ba
	go.uber.org/zap v1.18.1
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.27.1
)

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.12.3
// source: infrastructure/proto/message/aliyun_push/aliyun.proto

package aliyun_push

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	base "gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/his_api/base"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type BingAliasReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId      string `protobuf:"bytes,1,opt,name=userId,proto3" json:"userId,omitempty"`
	DeviceId    string `protobuf:"bytes,2,opt,name=deviceId,proto3" json:"deviceId,omitempty"`
	RequestType string `protobuf:"bytes,3,opt,name=requestType,proto3" json:"requestType,omitempty"`
	Lang        string `protobuf:"bytes,4,opt,name=lang,proto3" json:"lang,omitempty"`
}

func (x *BingAliasReq) Reset() {
	*x = BingAliasReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BingAliasReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BingAliasReq) ProtoMessage() {}

func (x *BingAliasReq) ProtoReflect() protoreflect.Message {
	mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BingAliasReq.ProtoReflect.Descriptor instead.
func (*BingAliasReq) Descriptor() ([]byte, []int) {
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP(), []int{0}
}

func (x *BingAliasReq) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

func (x *BingAliasReq) GetDeviceId() string {
	if x != nil {
		return x.DeviceId
	}
	return ""
}

func (x *BingAliasReq) GetRequestType() string {
	if x != nil {
		return x.RequestType
	}
	return ""
}

func (x *BingAliasReq) GetLang() string {
	if x != nil {
		return x.Lang
	}
	return ""
}

type BingAliasResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	RequestId string `protobuf:"bytes,2,opt,name=requestId,proto3" json:"requestId,omitempty"`
}

func (x *BingAliasResp) Reset() {
	*x = BingAliasResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BingAliasResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BingAliasResp) ProtoMessage() {}

func (x *BingAliasResp) ProtoReflect() protoreflect.Message {
	mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BingAliasResp.ProtoReflect.Descriptor instead.
func (*BingAliasResp) Descriptor() ([]byte, []int) {
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP(), []int{1}
}

func (x *BingAliasResp) GetRequestId() string {
	if x != nil {
		return x.RequestId
	}
	return ""
}

type GetAliasResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AliasName []string `protobuf:"bytes,1,rep,name=aliasName,proto3" json:"aliasName,omitempty"`
}

func (x *GetAliasResp) Reset() {
	*x = GetAliasResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAliasResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAliasResp) ProtoMessage() {}

func (x *GetAliasResp) ProtoReflect() protoreflect.Message {
	mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAliasResp.ProtoReflect.Descriptor instead.
func (*GetAliasResp) Descriptor() ([]byte, []int) {
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP(), []int{2}
}

func (x *GetAliasResp) GetAliasName() []string {
	if x != nil {
		return x.AliasName
	}
	return nil
}

type QueryPushReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	RequestType string `protobuf:"bytes,1,opt,name=requestType,proto3" json:"requestType,omitempty"`
	StartAt     string `protobuf:"bytes,2,opt,name=startAt,proto3" json:"startAt,omitempty"`
	EndAt       string `protobuf:"bytes,3,opt,name=endAt,proto3" json:"endAt,omitempty"`
	Page        int32  `protobuf:"varint,4,opt,name=Page,proto3" json:"Page,omitempty"`
	Size        int32  `protobuf:"varint,5,opt,name=Size,proto3" json:"Size,omitempty"`
}

func (x *QueryPushReq) Reset() {
	*x = QueryPushReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QueryPushReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QueryPushReq) ProtoMessage() {}

func (x *QueryPushReq) ProtoReflect() protoreflect.Message {
	mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QueryPushReq.ProtoReflect.Descriptor instead.
func (*QueryPushReq) Descriptor() ([]byte, []int) {
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP(), []int{3}
}

func (x *QueryPushReq) GetRequestType() string {
	if x != nil {
		return x.RequestType
	}
	return ""
}

func (x *QueryPushReq) GetStartAt() string {
	if x != nil {
		return x.StartAt
	}
	return ""
}

func (x *QueryPushReq) GetEndAt() string {
	if x != nil {
		return x.EndAt
	}
	return ""
}

func (x *QueryPushReq) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *QueryPushReq) GetSize() int32 {
	if x != nil {
		return x.Size
	}
	return 0
}

type QueryPushResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data       []*QueryPushResp_Data `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
	Pagination *base.Pagination      `protobuf:"bytes,2,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *QueryPushResp) Reset() {
	*x = QueryPushResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QueryPushResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QueryPushResp) ProtoMessage() {}

func (x *QueryPushResp) ProtoReflect() protoreflect.Message {
	mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QueryPushResp.ProtoReflect.Descriptor instead.
func (*QueryPushResp) Descriptor() ([]byte, []int) {
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP(), []int{4}
}

func (x *QueryPushResp) GetData() []*QueryPushResp_Data {
	if x != nil {
		return x.Data
	}
	return nil
}

func (x *QueryPushResp) GetPagination() *base.Pagination {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type QueryPushResp_Data struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AcceptCount int64  `protobuf:"varint,1,opt,name=acceptCount,proto3" json:"acceptCount,omitempty"` //接收数目/送达
	OpenedCount int64  `protobuf:"varint,2,opt,name=openedCount,proto3" json:"openedCount,omitempty"` //打开数量
	SentCount   int64  `protobuf:"varint,3,opt,name=sentCount,proto3" json:"sentCount,omitempty"`     //推送数量
	AcceptRate  string `protobuf:"bytes,4,opt,name=acceptRate,proto3" json:"acceptRate,omitempty"`    //打开/送达
	SentRate    string `protobuf:"bytes,5,opt,name=sentRate,proto3" json:"sentRate,omitempty"`        //打开/推送
	Date        string `protobuf:"bytes,6,opt,name=Date,proto3" json:"Date,omitempty"`
}

func (x *QueryPushResp_Data) Reset() {
	*x = QueryPushResp_Data{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QueryPushResp_Data) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QueryPushResp_Data) ProtoMessage() {}

func (x *QueryPushResp_Data) ProtoReflect() protoreflect.Message {
	mi := &file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QueryPushResp_Data.ProtoReflect.Descriptor instead.
func (*QueryPushResp_Data) Descriptor() ([]byte, []int) {
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP(), []int{4, 0}
}

func (x *QueryPushResp_Data) GetAcceptCount() int64 {
	if x != nil {
		return x.AcceptCount
	}
	return 0
}

func (x *QueryPushResp_Data) GetOpenedCount() int64 {
	if x != nil {
		return x.OpenedCount
	}
	return 0
}

func (x *QueryPushResp_Data) GetSentCount() int64 {
	if x != nil {
		return x.SentCount
	}
	return 0
}

func (x *QueryPushResp_Data) GetAcceptRate() string {
	if x != nil {
		return x.AcceptRate
	}
	return ""
}

func (x *QueryPushResp_Data) GetSentRate() string {
	if x != nil {
		return x.SentRate
	}
	return ""
}

func (x *QueryPushResp_Data) GetDate() string {
	if x != nil {
		return x.Date
	}
	return ""
}

var File_infrastructure_proto_message_aliyun_push_aliyun_proto protoreflect.FileDescriptor

var file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDesc = []byte{
	0x0a, 0x35, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65,
	0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x61,
	0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75, 0x73, 0x68, 0x2f, 0x61, 0x6c, 0x69, 0x79, 0x75,
	0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0b, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f,
	0x70, 0x75, 0x73, 0x68, 0x1a, 0x2c, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x73, 0x74, 0x72, 0x75, 0x63,
	0x74, 0x75, 0x72, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x68, 0x69, 0x73, 0x5f, 0x61,
	0x70, 0x69, 0x2f, 0x62, 0x61, 0x73, 0x65, 0x2f, 0x62, 0x61, 0x73, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x22, 0x78, 0x0a, 0x0c, 0x42, 0x69, 0x6e, 0x67, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52,
	0x65, 0x71, 0x12, 0x16, 0x0a, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x64, 0x65,
	0x76, 0x69, 0x63, 0x65, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x64, 0x65,
	0x76, 0x69, 0x63, 0x65, 0x49, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x54, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x72, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x22, 0x2d, 0x0a, 0x0d,
	0x42, 0x69, 0x6e, 0x67, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65, 0x73, 0x70, 0x12, 0x1c, 0x0a,
	0x09, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x49, 0x64, 0x22, 0x2c, 0x0a, 0x0c, 0x47,
	0x65, 0x74, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65, 0x73, 0x70, 0x12, 0x1c, 0x0a, 0x09, 0x61,
	0x6c, 0x69, 0x61, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x09,
	0x61, 0x6c, 0x69, 0x61, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x22, 0x88, 0x01, 0x0a, 0x0c, 0x51, 0x75,
	0x65, 0x72, 0x79, 0x50, 0x75, 0x73, 0x68, 0x52, 0x65, 0x71, 0x12, 0x20, 0x0a, 0x0b, 0x72, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x54, 0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x18, 0x0a, 0x07,
	0x73, 0x74, 0x61, 0x72, 0x74, 0x41, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73,
	0x74, 0x61, 0x72, 0x74, 0x41, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6e, 0x64, 0x41, 0x74, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6e, 0x64, 0x41, 0x74, 0x12, 0x12, 0x0a, 0x04,
	0x50, 0x61, 0x67, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x50, 0x61, 0x67, 0x65,
	0x12, 0x12, 0x0a, 0x04, 0x53, 0x69, 0x7a, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04,
	0x53, 0x69, 0x7a, 0x65, 0x22, 0xb1, 0x02, 0x0a, 0x0d, 0x51, 0x75, 0x65, 0x72, 0x79, 0x50, 0x75,
	0x73, 0x68, 0x52, 0x65, 0x73, 0x70, 0x12, 0x33, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x1f, 0x2e, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75,
	0x73, 0x68, 0x2e, 0x51, 0x75, 0x65, 0x72, 0x79, 0x50, 0x75, 0x73, 0x68, 0x52, 0x65, 0x73, 0x70,
	0x2e, 0x44, 0x61, 0x74, 0x61, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x12, 0x30, 0x0a, 0x0a, 0x70,
	0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x10, 0x2e, 0x62, 0x61, 0x73, 0x65, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x1a, 0xb8, 0x01,
	0x0a, 0x04, 0x44, 0x61, 0x74, 0x61, 0x12, 0x20, 0x0a, 0x0b, 0x61, 0x63, 0x63, 0x65, 0x70, 0x74,
	0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x61, 0x63, 0x63,
	0x65, 0x70, 0x74, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x20, 0x0a, 0x0b, 0x6f, 0x70, 0x65, 0x6e,
	0x65, 0x64, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x6f,
	0x70, 0x65, 0x6e, 0x65, 0x64, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x73, 0x65,
	0x6e, 0x74, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x73,
	0x65, 0x6e, 0x74, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x61, 0x63, 0x63, 0x65,
	0x70, 0x74, 0x52, 0x61, 0x74, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x61, 0x63,
	0x63, 0x65, 0x70, 0x74, 0x52, 0x61, 0x74, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x73, 0x65, 0x6e, 0x74,
	0x52, 0x61, 0x74, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x73, 0x65, 0x6e, 0x74,
	0x52, 0x61, 0x74, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x44, 0x61, 0x74, 0x65, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x44, 0x61, 0x74, 0x65, 0x32, 0xe8, 0x01, 0x0a, 0x09, 0x42, 0x69, 0x6e,
	0x67, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x12, 0x44, 0x0a, 0x09, 0x42, 0x69, 0x6e, 0x67, 0x41, 0x6c,
	0x69, 0x61, 0x73, 0x12, 0x19, 0x2e, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75, 0x73,
	0x68, 0x2e, 0x42, 0x69, 0x6e, 0x67, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65, 0x71, 0x1a, 0x1a,
	0x2e, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75, 0x73, 0x68, 0x2e, 0x42, 0x69, 0x6e,
	0x67, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65, 0x73, 0x70, 0x22, 0x00, 0x12, 0x46, 0x0a, 0x0c,
	0x47, 0x65, 0x74, 0x42, 0x69, 0x6e, 0x67, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x12, 0x19, 0x2e, 0x61,
	0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75, 0x73, 0x68, 0x2e, 0x42, 0x69, 0x6e, 0x67, 0x41,
	0x6c, 0x69, 0x61, 0x73, 0x52, 0x65, 0x71, 0x1a, 0x19, 0x2e, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e,
	0x5f, 0x70, 0x75, 0x73, 0x68, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65,
	0x73, 0x70, 0x22, 0x00, 0x12, 0x4d, 0x0a, 0x12, 0x51, 0x75, 0x65, 0x72, 0x79, 0x50, 0x75, 0x73,
	0x68, 0x53, 0x74, 0x61, 0x74, 0x42, 0x79, 0x41, 0x70, 0x70, 0x12, 0x19, 0x2e, 0x61, 0x6c, 0x69,
	0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75, 0x73, 0x68, 0x2e, 0x51, 0x75, 0x65, 0x72, 0x79, 0x50, 0x75,
	0x73, 0x68, 0x52, 0x65, 0x71, 0x1a, 0x1a, 0x2e, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70,
	0x75, 0x73, 0x68, 0x2e, 0x51, 0x75, 0x65, 0x72, 0x79, 0x50, 0x75, 0x73, 0x68, 0x52, 0x65, 0x73,
	0x70, 0x22, 0x00, 0x42, 0x4d, 0x5a, 0x4b, 0x67, 0x69, 0x74, 0x2e, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x74, 0x6f, 0x70, 0x2f, 0x62, 0x68, 0x65, 0x78, 0x2f,
	0x64, 0x66, 0x2d, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x73,
	0x74, 0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6d,
	0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x61, 0x6c, 0x69, 0x79, 0x75, 0x6e, 0x5f, 0x70, 0x75,
	0x73, 0x68, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescOnce sync.Once
	file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescData = file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDesc
)

func file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescGZIP() []byte {
	file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescOnce.Do(func() {
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescData = protoimpl.X.CompressGZIP(file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescData)
	})
	return file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDescData
}

var file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_infrastructure_proto_message_aliyun_push_aliyun_proto_goTypes = []interface{}{
	(*BingAliasReq)(nil),       // 0: aliyun_push.BingAliasReq
	(*BingAliasResp)(nil),      // 1: aliyun_push.BingAliasResp
	(*GetAliasResp)(nil),       // 2: aliyun_push.GetAliasResp
	(*QueryPushReq)(nil),       // 3: aliyun_push.QueryPushReq
	(*QueryPushResp)(nil),      // 4: aliyun_push.QueryPushResp
	(*QueryPushResp_Data)(nil), // 5: aliyun_push.QueryPushResp.Data
	(*base.Pagination)(nil),    // 6: base.Pagination
}
var file_infrastructure_proto_message_aliyun_push_aliyun_proto_depIdxs = []int32{
	5, // 0: aliyun_push.QueryPushResp.data:type_name -> aliyun_push.QueryPushResp.Data
	6, // 1: aliyun_push.QueryPushResp.pagination:type_name -> base.Pagination
	0, // 2: aliyun_push.BingAlias.BingAlias:input_type -> aliyun_push.BingAliasReq
	0, // 3: aliyun_push.BingAlias.GetBingAlias:input_type -> aliyun_push.BingAliasReq
	3, // 4: aliyun_push.BingAlias.QueryPushStatByApp:input_type -> aliyun_push.QueryPushReq
	1, // 5: aliyun_push.BingAlias.BingAlias:output_type -> aliyun_push.BingAliasResp
	2, // 6: aliyun_push.BingAlias.GetBingAlias:output_type -> aliyun_push.GetAliasResp
	4, // 7: aliyun_push.BingAlias.QueryPushStatByApp:output_type -> aliyun_push.QueryPushResp
	5, // [5:8] is the sub-list for method output_type
	2, // [2:5] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_infrastructure_proto_message_aliyun_push_aliyun_proto_init() }
func file_infrastructure_proto_message_aliyun_push_aliyun_proto_init() {
	if File_infrastructure_proto_message_aliyun_push_aliyun_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BingAliasReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BingAliasResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAliasResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QueryPushReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QueryPushResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QueryPushResp_Data); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_infrastructure_proto_message_aliyun_push_aliyun_proto_goTypes,
		DependencyIndexes: file_infrastructure_proto_message_aliyun_push_aliyun_proto_depIdxs,
		MessageInfos:      file_infrastructure_proto_message_aliyun_push_aliyun_proto_msgTypes,
	}.Build()
	File_infrastructure_proto_message_aliyun_push_aliyun_proto = out.File
	file_infrastructure_proto_message_aliyun_push_aliyun_proto_rawDesc = nil
	file_infrastructure_proto_message_aliyun_push_aliyun_proto_goTypes = nil
	file_infrastructure_proto_message_aliyun_push_aliyun_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// BingAliasClient is the client API for BingAlias service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type BingAliasClient interface {
	BingAlias(ctx context.Context, in *BingAliasReq, opts ...grpc.CallOption) (*BingAliasResp, error)
	GetBingAlias(ctx context.Context, in *BingAliasReq, opts ...grpc.CallOption) (*GetAliasResp, error)
	QueryPushStatByApp(ctx context.Context, in *QueryPushReq, opts ...grpc.CallOption) (*QueryPushResp, error)
}

type bingAliasClient struct {
	cc grpc.ClientConnInterface
}

func NewBingAliasClient(cc grpc.ClientConnInterface) BingAliasClient {
	return &bingAliasClient{cc}
}

func (c *bingAliasClient) BingAlias(ctx context.Context, in *BingAliasReq, opts ...grpc.CallOption) (*BingAliasResp, error) {
	out := new(BingAliasResp)
	err := c.cc.Invoke(ctx, "/aliyun_push.BingAlias/BingAlias", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *bingAliasClient) GetBingAlias(ctx context.Context, in *BingAliasReq, opts ...grpc.CallOption) (*GetAliasResp, error) {
	out := new(GetAliasResp)
	err := c.cc.Invoke(ctx, "/aliyun_push.BingAlias/GetBingAlias", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *bingAliasClient) QueryPushStatByApp(ctx context.Context, in *QueryPushReq, opts ...grpc.CallOption) (*QueryPushResp, error) {
	out := new(QueryPushResp)
	err := c.cc.Invoke(ctx, "/aliyun_push.BingAlias/QueryPushStatByApp", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// BingAliasServer is the server API for BingAlias service.
type BingAliasServer interface {
	BingAlias(context.Context, *BingAliasReq) (*BingAliasResp, error)
	GetBingAlias(context.Context, *BingAliasReq) (*GetAliasResp, error)
	QueryPushStatByApp(context.Context, *QueryPushReq) (*QueryPushResp, error)
}

// UnimplementedBingAliasServer can be embedded to have forward compatible implementations.
type UnimplementedBingAliasServer struct {
}

func (*UnimplementedBingAliasServer) BingAlias(context.Context, *BingAliasReq) (*BingAliasResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BingAlias not implemented")
}
func (*UnimplementedBingAliasServer) GetBingAlias(context.Context, *BingAliasReq) (*GetAliasResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetBingAlias not implemented")
}
func (*UnimplementedBingAliasServer) QueryPushStatByApp(context.Context, *QueryPushReq) (*QueryPushResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method QueryPushStatByApp not implemented")
}

func RegisterBingAliasServer(s *grpc.Server, srv BingAliasServer) {
	s.RegisterService(&_BingAlias_serviceDesc, srv)
}

func _BingAlias_BingAlias_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BingAliasReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BingAliasServer).BingAlias(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/aliyun_push.BingAlias/BingAlias",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BingAliasServer).BingAlias(ctx, req.(*BingAliasReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _BingAlias_GetBingAlias_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BingAliasReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BingAliasServer).GetBingAlias(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/aliyun_push.BingAlias/GetBingAlias",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BingAliasServer).GetBingAlias(ctx, req.(*BingAliasReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _BingAlias_QueryPushStatByApp_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(QueryPushReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BingAliasServer).QueryPushStatByApp(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/aliyun_push.BingAlias/QueryPushStatByApp",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BingAliasServer).QueryPushStatByApp(ctx, req.(*QueryPushReq))
	}
	return interceptor(ctx, in, info, handler)
}

var _BingAlias_serviceDesc = grpc.ServiceDesc{
	ServiceName: "aliyun_push.BingAlias",
	HandlerType: (*BingAliasServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "BingAlias",
			Handler:    _BingAlias_BingAlias_Handler,
		},
		{
			MethodName: "GetBingAlias",
			Handler:    _BingAlias_GetBingAlias_Handler,
		},
		{
			MethodName: "QueryPushStatByApp",
			Handler:    _BingAlias_QueryPushStatByApp_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "infrastructure/proto/message/aliyun_push/aliyun.proto",
}

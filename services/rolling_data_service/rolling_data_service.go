package rolling_data_service

import (
	"context"
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-common/grpc_client/rolling_data_api_client"
	rollingData "gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/rollingdata_api/register"
	"gitlab.yuntron.org/backend/core/log"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net"
)

type RollingDataService struct {
	grpcClient rolling_data_api_client.RollingDataClient
	grpcServer rollingData.UnimplementedRollingDataServer
}

var RollingDataServiceInstance = &RollingDataService{}

func (r *RollingDataService) InitClient(host, port string) {
	r.grpcClient.Init(host, port)
}

func (r *RollingDataService) StartServer(port string) {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.ZapLogger.Error(fmt.Sprintf("failed to listen: %v", err))
	}
	s := grpc.NewServer()
	rollingData.RegisterRollingDataServer(s, &r.grpcServer)
	if err := s.Serve(lis); err != nil {
		log.ZapLogger.Error(fmt.Sprintf("failed to serve: %v", err))
	}
}

func (u *RollingDataService) Subscribe(symbol, ruleName string, limit float64) *rollingData.RegisterResp {
	if client, err := u.grpcClient.RollingDataClient(); err == nil {
		ctx := context.Background()
		resp, err := client.Register(ctx, &rollingData.RegisterReq{
			Symbol:   symbol,
			RuleName: ruleName,
			Limit:    limit,
		})
		if err != nil {
			log.ZapLogger.With(zap.String("err", err.Error())).Error(fmt.Sprintf("订阅%s %s 失败", ruleName, symbol))
		} else {
			return resp
		}
	} else {
		log.ZapLogger.With(zap.String("err", err.Error())).Error("调用grpc 服务失败")
		return nil
	}
	return nil
}

func (u *RollingDataService) UnSubscribe(symbol, ruleName string) *rollingData.RegisterResp {
	if client, err := u.grpcClient.RollingDataClient(); err == nil {
		ctx := context.Background()
		resp, err := client.UnRegister(ctx, &rollingData.UnRegisterReq{
			Symbol:   symbol,
			RuleName: ruleName,
		})
		if err != nil {
			log.ZapLogger.With(zap.String("err", err.Error())).Error(fmt.Sprintf("取消订阅%s %s 失败", ruleName, symbol))
		} else {
			return resp
		}
	} else {
		log.ZapLogger.With(zap.String("err", err.Error())).Error("调用grpc 服务失败")
		return nil
	}
	return nil
}

package quotes_service

import (
	"fmt"
	"gitlab.yuntron.org/backend/core/log"
	"gitlab.yuntron.org/backend/core/mq"
	"testing"
)

func TestQuotesService_GetTickerPrice(t *testing.T) {
	q := QuotesService{}
	q.Init(false, nil)
	tickers, err := q.GetSymbolTickerPrice()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("ticker price  %v", tickers)
	fmt.Println()
	allRates, err := q.GetAllRates()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("all rates %v", allRates)
	fmt.Println()

	rates, err := q.GetRates("BTC", "USDT")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf(" rates %v", rates)
	fmt.Println()

	tokens, err := q.GetTokens()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("tokens %v", tokens)
	fmt.Println()

	symbols, err := q.GetSymbols()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("symbols %v", symbols)
	fmt.Println()
}

func TestSubscribeNewPrice(t *testing.T) {
	err := log.Init("debug", "test_subscribe_new_price")
	if err != nil {
		t.Fatalf("init log has error %s", err.Error())
	}

	priceChan, err := SubscribeNewPrice("test_new_price_group", "tick", []string{"127.0.0.1:9092"})
	if err != nil {
		t.Fatalf("subscribe new price has error %s", err.Error())
	}

	for i := 0; i < 10; i++ {
		select {
		case data := <-priceChan:
			t.Logf("new price %+v", data)
		}
	}

	err = mq.KafkaConsumerGroup.Close()
	if err != nil {
		t.Logf("kafka consumer product close err %s", err.Error())
	}
}

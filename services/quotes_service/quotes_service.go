package quotes_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/patrickmn/go-cache"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/quotation/tick"
	"gitlab.yuntron.org/backend/core/http/simple_http"
	"gitlab.yuntron.org/backend/core/log"
	"gitlab.yuntron.org/backend/core/mq"
	"time"
)

type QuotesUrl struct {
	Rates       string `yaml:"rates" mapstructure:"rates"`
	TickerPrice string `yaml:"ticker_price" mapstructure:"ticker_price"`
	AllRates    string `yaml:"rates" mapstructure:"all_rates"`
	Tokens      string `yaml:"tokens" mapstructure:"tokens"`
	Symbols     string `yaml:"symbols" mapstructure:"symbols"`
}

type RateItem struct {
	Token string            `json:"token"`
	Rates map[string]string `json:"rates"`
}

type AllRateItem struct {
	TokenID   string `json:"tokenId"`
	TokenName string `json:"tokenName"`
	Time      string `json:"time"`
	CNY       string `json:"cny"`
	USD       string `json:"usd"`
	BTC       string `json:"btc"`
	USDT      string `json:"usdt"`
	ETH       string `json:"eth"`
}

type QuotesService struct {
	EnableMemoryCache bool
	QuotesUrl         *QuotesUrl
	cache             *cache.Cache
}

type RatesResponse struct {
	Code    int        `json:"code"`
	Data    []RateItem `json:"data"`
	Message string     `json:"msg"`
}

type AllRatesResponse struct {
	Code    int           `json:"code"`
	Data    []AllRateItem `json:"data"`
	Message string        `json:"msg"`
}

var QuotesServiceInstance = &QuotesService{}

func (q *QuotesService) Init(enableMemoryCache bool, url *QuotesUrl) error {
	q.EnableMemoryCache = enableMemoryCache
	if url == nil {
		url = &QuotesUrl{
			Rates:       "https://www.zg.com/api/quote/v1/rates",
			TickerPrice: "https://www.zg.com/openapi/quote/v1/ticker/price",
			AllRates:    "https://www.zg.com/api/basic/rates",
			Tokens:      "https://www.zg.com/api/basic/tokens",
			Symbols:     "https://www.zg.com/api/basic/symbols",
		}
	}
	q.QuotesUrl = url
	if q.EnableMemoryCache {
		q.cache = cache.New(cache.DefaultExpiration, 30*time.Second)
	}
	return nil
}

func (q *QuotesService) CrontabStart() {
	if q.cache == nil {
		q.cache = cache.New(cache.DefaultExpiration, 30*time.Second)
	}
	q.startFetchQuotes()
}

func (q *QuotesService) startFetchQuotes() {
	timer := time.NewTicker(500 * time.Millisecond)
	go func(t *time.Ticker, q *QuotesService) {
		for {
			<-t.C

		}
	}(timer, q)
}

//获取汇率
func (q *QuotesService) GetRates(tokens string, legalCoins string) (map[string]RateItem, error) {
	timeOut := 5 * time.Second
	resp, body, errs := simple_http.Get(q.QuotesUrl.Rates, nil, map[string]interface{}{
		"tokens":     tokens,
		"legalCoins": legalCoins,
	}, &timeOut)
	if len(errs) != 0 || resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code %v  error %v", resp.StatusCode, errs[0]))
	}
	rateResponse := RatesResponse{}
	err := json.Unmarshal(body, &rateResponse)
	if err != nil {
		return nil, err
	}
	if rateResponse.Code != 0 {
		return nil, errors.New(fmt.Sprintf("resp code %v,msg %v", rateResponse.Code, rateResponse.Message))
	}
	rateMap := map[string]RateItem{}
	for _, v := range rateResponse.Data {
		rateMap[v.Token] = v
	}
	return rateMap, nil
}

func (q *QuotesService) GetAllRates() (map[string]AllRateItem, error) {
	timeOut := 5 * time.Second
	resp, body, errs := simple_http.Get(q.QuotesUrl.AllRates, nil, nil, &timeOut)
	if len(errs) != 0 || resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code %v  error %v", resp.StatusCode, errs[0]))
	}
	rates := []AllRateItem{}
	err := json.Unmarshal(body, &rates)
	if err != nil {
		return nil, err
	}
	rateMap := map[string]AllRateItem{}
	for _, v := range rates {
		rateMap[v.TokenID] = v
	}
	return rateMap, nil
}

type TickerPriceItem struct {
	Symbol string `json:"symbol"`
	Price  string `json:"price"`
}

//获取现货价格【注意：非指数】
func (q *QuotesService) GetSymbolTickerPrice() ([]TickerPriceItem, error) {
	timeOut := 5 * time.Second
	resp, body, errs := simple_http.Get(q.QuotesUrl.TickerPrice, nil, nil, &timeOut)
	if len(errs) != 0 || resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code %v  error %v", resp.StatusCode, errs[0]))
	}
	tickerPriceResponse := []TickerPriceItem{}
	err := json.Unmarshal(body, &tickerPriceResponse)
	if err != nil {
		return nil, err
	}
	return tickerPriceResponse, nil
}

type Symbol struct {
	OrgID             string        `json:"orgId"`
	TokenID           string        `json:"tokenId"`
	TokenName         string        `json:"tokenName"`
	TokenFullName     string        `json:"tokenFullName"`
	IconURL           string        `json:"iconUrl"`
	AllowWithdraw     bool          `json:"allowWithdraw"`
	AllowDeposit      bool          `json:"allowDeposit"`
	BaseTokenSymbols  []TokenSymbol `json:"baseTokenSymbols"`
	QuoteTokenSymbols []TokenSymbol `json:"quoteTokenSymbols"`
	IsEOS             bool          `json:"isEOS"`
	TokenType         string        `json:"tokenType"`
	NeedAddressTag    bool          `json:"needAddressTag"`
	NeedKycQuantity   string        `json:"needKycQuantity"`
	CustomOrder       int           `json:"customOrder"`
	ChainTypes        []ChainTypes  `json:"chainTypes"`
}

type TokenSymbol struct {
	OrgID               string `json:"orgId"`
	ExchangeID          string `json:"exchangeId"`
	SymbolID            string `json:"symbolId"`
	SymbolName          string `json:"symbolName"`
	BaseTokenID         string `json:"baseTokenId"`
	BaseTokenName       string `json:"baseTokenName"`
	QuoteTokenID        string `json:"quoteTokenId"`
	QuoteTokenName      string `json:"quoteTokenName"`
	BasePrecision       string `json:"basePrecision,omitempty"`
	QuotePrecision      string `json:"quotePrecision,omitempty"`
	MinTradeQuantity    string `json:"minTradeQuantity,omitempty"`
	MinTradeAmount      string `json:"minTradeAmount,omitempty"`
	MinPricePrecision   string `json:"minPricePrecision,omitempty"`
	DigitMerge          string `json:"digitMerge,omitempty"`
	CanTrade            bool   `json:"canTrade,omitempty"`
	CustomOrder         int    `json:"customOrder,omitempty"`
	IndexRecommendOrder int    `json:"indexRecommendOrder,omitempty"`
	ShowStatus          bool   `json:"showStatus,omitempty"`
	Category            int    `json:"category,omitempty"`
	AllowMargin         bool   `json:"allowMargin,omitempty"`
}

func (q *QuotesService) GetSymbols() (map[string]Symbol, error) {
	timeOut := 5 * time.Second
	resp, body, errs := simple_http.Get(q.QuotesUrl.Tokens, nil, nil, &timeOut)
	if len(errs) != 0 || resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code %v  error %v", resp.StatusCode, errs[0]))
	}
	result := []Symbol{}
	fmt.Println(string(body))
	err := json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}
	resultMap := map[string]Symbol{}
	for _, v := range result {
		resultMap[v.TokenID] = v
	}
	return resultMap, nil
}

type Token struct {
	TokenID           string        `json:"tokenId"`
	TokenName         string        `json:"tokenName"`
	TokenFullName     string        `json:"tokenFullName"`
	IconURL           string        `json:"iconUrl"`
	AllowWithdraw     bool          `json:"allowWithdraw"`
	AllowDeposit      bool          `json:"allowDeposit"`
	BaseTokenSymbols  []TokenSymbol `json:"baseTokenSymbols"`
	QuoteTokenSymbols []TokenSymbol `json:"quoteTokenSymbols"`
	IsEOS             bool          `json:"isEOS"`
	NeedAddressTag    bool          `json:"needAddressTag"`
	ChainTypes        []ChainTypes  `json:"chainTypes"`
}

type ChainTypes struct {
	ChainType     string `json:"chainType"`
	AllowDeposit  bool   `json:"allowDeposit"`
	AllowWithdraw bool   `json:"allowWithdraw"`
}

func (q *QuotesService) GetTokens() (map[string]Token, error) {
	timeOut := 5 * time.Second
	resp, body, errs := simple_http.Get(q.QuotesUrl.Tokens, nil, nil, &timeOut)
	if len(errs) != 0 || resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code %v  error %v", resp.StatusCode, errs[0]))
	}
	result := []Token{}
	err := json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}
	resultMap := map[string]Token{}
	for _, v := range result {
		resultMap[v.TokenID] = v
	}
	return resultMap, nil
}

// 订阅最新价格
func SubscribeNewPrice(group string, topic string, brokers []string) (chan *tick.NewTick, error) {
	config := &mq.KafkaConsumerGroupConf{
		GroupName:         group,
		Brokers:           brokers,
		Topics:            []string{topic},
		ResetNewestOffset: true,
	}

	newPriceChan := make(chan *tick.NewTick, 1024) // 最新价格通道

	messageHandler := func(topic string, value []byte) error {

		tickData := tick.NewTick{}
		err := proto.Unmarshal(value, &tickData)
		if err != nil {
			log.ZapLogger.Warn(fmt.Sprintf("proto unmarsha tick data has error %s", err.Error()))
			return err
		}
		newPriceChan <- &tickData
		return nil
	}

	err := mq.NewKafkaConsumerGroup(config, messageHandler)
	if err != nil {
		return nil, err
	}
	return newPriceChan, nil
}

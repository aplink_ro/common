package user_info_service

import (
	"context"
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-common/grpc_client/user_service_client"
	"gitlab.com/mini-tech/aptoken/aplink-common/infrastructure/proto/user_info/user_info"
	"gitlab.yuntron.org/backend/core/log"
	"go.uber.org/zap"
)

type UserInfoService struct {
	gprcClient user_info_client.UserInfoClient
}

var UserInfoServiceInstance = &UserInfoService{}

func (u *UserInfoService) Init(host, port string) {
	u.gprcClient.Init(host, port)
}

func (u *UserInfoService) GetUserInfo(userId string) *user_info.UserInfo {
	if client, err := u.gprcClient.GetUserInfoClient(); err == nil {
		ctx := context.Background()
		resp, err := client.GetUserInfo(ctx, &user_info.GetBaseInfoReq{
			UserId: userId,
		})
		if err != nil {
			log.ZapLogger.With(zap.String("err", err.Error())).Error("获取用户信息失败")
		} else {
			resp.NationalCode = fmt.Sprintf("+%s", resp.NationalCode)
			return resp
		}
	} else {
		log.ZapLogger.With(zap.String("err", err.Error())).Error("调用grpc 服务失败")
		return nil
	}
	return nil
}

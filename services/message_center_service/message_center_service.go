package message_center_service

import (
	"fmt"
	"gitlab.yuntron.org/backend/core/http/simple_http"
	"github.com/pkg/errors"
	"time"
)

type MsgCenterUrls struct {
	BaseUrl   string `json:"base_url"`
	SendSMS   string `json:"send_sms"`
	SendEmail string `json:"send_email"`
}

var MsgCenterServiceInstance = &MessageCenterService{}

type MessageCenterService struct {
	Urls *MsgCenterUrls
}

func (m *MessageCenterService) Init(urls *MsgCenterUrls) bool {
	if urls == nil {
		return false
	}
	m.Urls = urls
	return true
}

//发送短信
func (m *MessageCenterService) SendSMS(headers map[string]string, params map[string]interface{}) error {
	timeOut := 10 * time.Second
	resp, _, errs := simple_http.Post(m.Urls.BaseUrl+m.Urls.SendSMS, headers, params, &timeOut)
	if errs != nil || resp.StatusCode != 200 {
		return errors.New(fmt.Sprintf("send [%v%v] sms errors: %v; resp: %v", m.Urls.BaseUrl+m.Urls.SendSMS, errs, resp))
	}
	return nil
}

func (m *MessageCenterService) SendEmail(headers map[string]string, params map[string]interface{}) error {
	timeOut := 10 * time.Second
	resp, _, errs := simple_http.Post(m.Urls.BaseUrl+m.Urls.SendEmail, headers, params, &timeOut)
	if errs != nil || resp.StatusCode != 200 {
		return errors.New(fmt.Sprintf("send [%v%v] email errors: %v; resp: %v", m.Urls.BaseUrl+m.Urls.SendEmail, errs, resp))
	}
	return nil
}

package error_code

// 错误码命名规则：{服务名首单词前3字母}_{服务名第二单词首字母}_{错误描述}
const (
	ERROR                uint64 = 0    // 内部错误
	INTERNAL_ERROR       uint64 = 1000 // 内部错误
	PARAM_ERROR          uint64 = 1001 // 参数错误
	REDIS_ERROR          uint64 = 1002 // 内部错误
	MYSQL_ERROR          uint64 = 1003 // 内部错误
	JSON_PARSE_FAIL      uint64 = 1004 // 内部错误
	JWT_VERIFY_ERROR     uint64 = 1005 // jwt权限验证失败
	API_RATELIMIT        uint64 = 1006 // api频率限制
	JWT_RIGHT_NOT_ENOUGH uint64 = 1007 // jwt权限不足
	ERROR_JWT_GENERATE   uint64 = 1008 // jwt生成错误

	NOTICE_ADD_SINGLE_LIMITED uint64 = 1101 // 用户添加单币对（或指数合约）价格提醒数量超限
	NOTICE_ADD_TOTAL_LIMITED  uint64 = 1102 // 用户添加价格提醒数量总数超限

	WAL_S_CURRENCY_NOT_SUPPORT uint64 = 2000 // 币种不支持
	WAL_S_WITHDRAW_STOP        uint64 = 2001 // 提现已暂停
	WAL_S_BALANCE_NOT_ENOUGH   uint64 = 2002 // 余额不足
	WAL_S_WITHDRAW_FORBID      uint64 = 2003 // 禁止提现
	WAL_S_ADDRESS_ILLEGAL      uint64 = 2005 // 地址非法
	WAL_S_GET_RATE_ERROR       uint64 = 2009 // 获取汇率失败
	WAL_S_QUOTA_DAILY_LIMIT    uint64 = 2010 // 到达每日限额
	WAL_S_QUOTA_TOTAL_LIMIT    uint64 = 2011 // 到达总限额
	WAL_S_AMOUNT_TOO_MUCH      uint64 = 2012 // 提现数量太大
	WAL_S_SPOT_FORBIDDEN       uint64 = 2013 // 用户被禁止币币提现
	WAL_S_SECURITY_SET_LIMIT   uint64 = 2014 // 安全设置24小时内不能提现
	WAL_S_ADDRESS_EXISTS       uint64 = 2015 // 提现地址已存在
	WAL_S_ADDRESS_POOL_NO_ADDR uint64 = 2016 // 地址池地址不够
	WAL_S_AMOUNT_DECIMAL_ERR   uint64 = 2017 // 提币数量精度有问题
	WAL_S_WALLET_CONFIG_ERR    uint64 = 2018 // 钱包配置有误
	WAL_S_WITHDRAW_NEED_TAG    uint64 = 2019 // 提币需要tag
	WAL_S_TAG_TOO_LONG         uint64 = 2020 // tag太长

	USE_S_VCODE_ERROR                       uint64 = 3000 //验证码错误
	USE_S_ACCOUNT_ALREADY_EXIST             uint64 = 3001 //用户已存在
	USE_S_CREATE_ENGINE_ACCOUNT_FAIL        uint64 = 3002 //创建引擎账户失败
	USE_S_CREATE_JWT_FAIL                   uint64 = 3003 //创建jwt失败
	USE_S_ACCOUNT_NOT_EXIST                 uint64 = 3004 //用户不存在
	USE_S_LOGIN_FAIL                        uint64 = 3005 //登陆失败
	USE_S_ENGINE_ACCOUNT_NOT_EXSIST         uint64 = 3006 //引擎账户缺失
	USE_S_ENGINE_ACCOUNT_ALREADY_EXIST      uint64 = 3007 //引擎账户已存在
	USE_S_CONFIG_NOT_EXIST                  uint64 = 3008 //配置不存在
	USE_S_GACODE_ALREADY_UESD               uint64 = 3009 //谷歌验证码已经使用了
	USE_S_GACODE_VERIFY_FAILED              uint64 = 3010 //谷歌验证码验证失败
	USE_S_TRADE_PWD_VERIFY_FAILED           uint64 = 3011 //交易密码验证失败
	USE_S_SMSCODE_VERIFY_FAILED             uint64 = 3012 //手机验证码验证失败
	USE_S_EMAILCODE_VERIFY_FAILED           uint64 = 3013 //邮箱验证码验证失败
	USE_S_SECURITY_VERIFY_FAILED            uint64 = 3014 //安全验证失败
	USE_S_ALREADY_BINDED                    uint64 = 3015 //用户已经绑定
	USE_S_MOBILE_OR_EMAIL_MUST_HAS_ONE_OPEN uint64 = 3016 //手机或者邮箱必须有一个开启
	USE_S_ALREADY_KYC                       uint64 = 3017 //用户已经实名验证
	USE_S_ONE_LEVEL_KYC_MUST_AUTH           uint64 = 3018 //必须通过一级实名验证
	USE_S_KYC_INFO_NOT_FOUND                uint64 = 3019 //用户认证信息没有找到
	USE_S_EXCEPTIONAL_USER                  uint64 = 3020 //用户异常
	USE_S_CAPTCHA_ERROR                     uint64 = 3021 //图形验证码错误或失效
	USE_S_ONE_IDCARD_ERROR                  uint64 = 3022 //一级实名认证失败
	USE_S_FACEID_SIGN_FAILED                uint64 = 3023 //face++签名错误
	USE_S_SPONSOR_IS_VERIFYING_OR_USING     uint64 = 3024 //申请的保荐机构正在审核或者已经在使用
	USE_S_SPONSOR_NOT_APPLY                 uint64 = 3025 //还没有申请保荐机构
	USE_S_SPONSOR_NOT_APPLY_OR_VERIFY       uint64 = 3026 //还没有申请或者审核保荐机构
	USE_S_SPONSOR_RIGHT_NOT_EXIST           uint64 = 3027 //还没有配置保荐机构的权益
	USE_S_TRADE_PWD_MUST_SET                uint64 = 3028 //交易密码必须设置
	USE_S_INVITE_CODE_NOT_EXIST             uint64 = 3029 //邀请码不存在
	USE_S_HAS_NO_USING_SPONSOR              uint64 = 3030 //还没有正在使用的保荐机构
	USE_S_COBBER_IS_APPLYING                uint64 = 3031 //合伙人正在申请升级中
	USE_S_SECURITY_TYPE_ERROR               uint64 = 3032 // 安全验证类型错误
	USE_S_EMAIL_ALREADY_BINDED              uint64 = 3033 // 这个邮箱已经被绑定
	USE_S_CARDNUM_BINDED                    uint64 = 3034 // 身份证已经被别人绑定
	USE_S_UPLOAD_TYPE_ERROR                 uint64 = 3035 // 上传图片type错误
	USE_S_UPLOAD_FILE_ERROR                 uint64 = 3036 // 图片不合法
	USE_S_UPLOAD_IMAGETYPE_ERROR            uint64 = 3037 // 图片类型不合法
	USE_S_UPLOAD_IMAGE_TOO_BIG              uint64 = 3038 // 图片太大
	USE_S_KYC_PHOTO_MISSING                 uint64 = 3039 // 高级认证审核图片缺失
	USE_S_KYC_TYPE_ERROR                    uint64 = 3040 // 高级kyc类型错误
	USE_S_KYC_COUNTRY_ID_ERROR              uint64 = 3041 // 用户国籍有错误
	USE_S_KYC_FACE_CHECK_EXCEED_LIMIT       uint64 = 3042 // 用户face++认证失败超过限制
	USE_S_MOBILE_REGISTED                   uint64 = 3043 // 手机已经被注册
	USE_S_EMAIL_REGISTED                    uint64 = 3044 // 邮箱已经被注册
	USE_S_MOBILE_NOT_REGISTED               uint64 = 3045 // 手机未被注册
	USE_S_EMAIL_NOT_REGISTED                uint64 = 3046 // 邮箱未被注册
	USE_S_ARTICLE_NOT_EXIST                 uint64 = 3047 // 文章不存在
	USE_S_LOGIN_CHECK_TYPE_ERROR            uint64 = 3048 // 登录校验类型不正确
	USE_S_LOGIN_CHECK_CODE_EMPTY            uint64 = 3049 // 登录验证码不正确
	USE_S_USER_BANED                        uint64 = 3050 //用户被禁用
	USE_S_KYC_GET_TOKEN_ERROR               uint64 = 3051 //获取face++二维码失败
	USE_S_OLD_TOKEN_ERROR                   uint64 = 3052 // 旧token校验失败
	USE_S_REFRESH_TOKEN_ERROR               uint64 = 3053 // refresh token错误, 需要重新登录
	USE_S_MYKEY_LOGIN_VERIFY_ERROR          uint64 = 3054 // mykey登录签名验证失败
	USE_S_MYKEY_BOUND_ERROR                 uint64 = 3055 // mykey账户已经被绑定
	USE_S_MYKEY_LOGIN_EXPIRED               uint64 = 3056 // mykey登录过期
	USE_S_MYKEY_NOT_BOUND_CAN_REGIST        uint64 = 3057 // mykey已授权，但没有绑定
	USE_S_MYKEY_NOT_AUTHORIZE               uint64 = 3058 // mykey未授权
	USE_S_MYKEY_BOUND                       uint64 = 3509 // mykey账号已经被绑定

	API_S_GET_ENGINE_ACCOUNT_FAIL    uint64 = 4000 // 获取引擎账户信息失败
	API_S_ACCOUNT_BALANCE_NOT_ENOUGH uint64 = 4001 // 账户余额不足

	BRI_S_EMAIL_SERVICE_NOT_FOUND     uint64 = 5000 // 没找到邮箱服务供应商
	BRI_S_EMAIL_TEMPLATE_NOT_FOUND    uint64 = 5001 // 邮件模板未找到
	BRI_S_DINGDING_ROBOT_NOT_FOUND    uint64 = 5002 // 没找到dingding机器人
	BRI_S_DINGDING_ROBOT_SEND_FAIL    uint64 = 5003 // dingding机器人发送失败
	BRI_S_SMS_TYPE_NOT_EXISTS         uint64 = 5004 // 短信类型不存在
	BRI_S_VCODE_VERIFY_FAILED         uint64 = 5005 // 验证码验证失败
	BRI_S_EMAIL_TYPE_NOT_EXISTS       uint64 = 5006 // 邮件类型不存在
	BRI_S_SMS_SEND_FAIL               uint64 = 5007 // 短信发送失败（系统错误）
	BRI_S_SMS_SERVICE_NOT_FOUND       uint64 = 5008 // 没有找到短信服务供应商
	BRI_S_SMS_TEMPLATE_NOT_FOUND      uint64 = 5009 // 短信模板未找到
	BRI_S_KYC_FAIL                    uint64 = 5010 //身份信息二要素认证/银行四要素认证失败（其它未列出的错误）
	BRI_S_KYC_SERVICE_NOT_FOUND       uint64 = 5011 //未找到kyc认证服务商
	BRI_S_KYC_SERVICE_NOT_SUPPORT     uint64 = 5012 //不支持的kyc'认证服务商
	BRI_S_KYC_TOO_MANY_TIME           uint64 = 5013 //当前kyc认证信息被尝试次数过多
	BRI_S_KYC_BANK_NULL               uint64 = 5014 //银行卡号不能为空
	BRI_S_KYC_BANK_UNVALIED           uint64 = 5015 //银行卡号格式错误
	BRI_S_KYC_BANK_BIN_NOT_EXIST      uint64 = 5016 //银行卡卡BIN不存在
	BRI_S_KYC_NAME_NULL               uint64 = 5017 //姓名不能为空
	BRI_S_KYC_NAME_UNVALIED           uint64 = 5018 //姓名格式错误
	BRI_S_KYC_ID_CARD_NULL            uint64 = 5019 //证件号不能为空
	BRI_S_KYC_ID_CARD_UNVALIED        uint64 = 5020 //证件号格式错误
	BRI_S_KYC_MOBILE_NULL             uint64 = 5021 //手机号码不能为空
	BRI_S_KYC_MOBILE_UNVALIED         uint64 = 5022 //手机号码格式错误
	BRI_S_KYC_BANK_TRY_TOO_MANY       uint64 = 5023 //银行四要素_当日信息尝试验证次数过多 身份证号，手机号，银行卡号的其中一个相同
	BRI_S_KYC_API_ERROR               uint64 = 5024 //三方接口异常
	BRI_S_KYC_ID_CARD_NOT_EXIST       uint64 = 5025 //无此身份证号码
	BRI_S_KYC_ID_API_STOP             uint64 = 5026 //三方接口维护中，请稍后再试
	BRI_S_KYC_ID_FAIL                 uint64 = 5027 //身份证信息不匹配
	BRI_S_KYC_BANK_FAIL               uint64 = 5028 //银行四要素认证信息不一致
	BRI_S_MAIL_SEND_FAIL              uint64 = 5029 // 邮件发送失败（系统错误）
	BRI_S_MAIL_INVALID_TOADDRESS      uint64 = 5030 // 目标地址不正确
	BRI_S_MAIL_INVALID_TOADDRESS_SPAM uint64 = 5031 // 收件地址因无效而拒绝，请检查收件地址是否真实有效。
	BRI_S_SMS_MOBILE_NUMBER_ILLEGAL   uint64 = 5032 // 手机号码格式错误
	BRI_S_SMS_SEND_TOO_MANY           uint64 = 5033 // 发送短信频率过高，请稍后再试
	BRI_S_VCODE_NEED_CAPTCHA          uint64 = 5034 // 请输入正确图形验证码

	C2C_S_BANKCARD_AMOUNT_LIMIT uint64 = 6000 // 银行卡总数限制
	C2C_S_BANKCARD_EXISTS       uint64 = 6001 // 银行卡已经存在
	C2C_S_BANKCARD_NOT_EXISTS   uint64 = 6002 // 银行卡不存在
	C2C_S_GLOBAL_CONFIG_ERROR   uint64 = 6003 // 配置错误
	C2C_S_ORDER_NOT_EXISTS      uint64 = 6004 // 订单不存在
	C2C_S_ORDER_CANNOT_CANCEL   uint64 = 6005 // 订单不可取消
	C2C_S_PENDING_ORDER_EXIST   uint64 = 6006 // 已存在pending订单
	C2C_S_NO_SYSTEM_ACCOUNT     uint64 = 6007 // 没有系统银行账户
	C2C_S_BALANCE_NOT_ENOUGH    uint64 = 6008 // 余额不足
	C2C_S_QUOTA_TOTAL_LIMIT     uint64 = 6009 // 达到总限额
	C2C_S_C2C_FORBIDDEN         uint64 = 6010 // 用户c2c被禁用
	C2C_S_NEED_KYC              uint64 = 6011 // 需要先进行一级kyc
	C2C_S_BANKCARD_BINDED       uint64 = 6012 // 银行卡已经被别人绑定

	ENG_S_ENGINE_DEDUCT_WITHDRAW_FREEZE_FAIL uint64 = 7000 // 扣除冻结失败
	ENG_S_ENGINE_WITHDRAW_FREEZE_FAIL        uint64 = 7001 // 冻结失败
	ENG_S_ENGINE_DEPOSIT_FAIL                uint64 = 7002 // 充值失败

	STO_S_UPLOAD_FAIL uint64 = 8000 // 图片上传失败

	IEO_S_REDEEM_CODE_ALREADY_ACTIVATED uint64 = 9001 //该兑换码已经激活
	IEO_S_TEAM_FULL                     uint64 = 9002 //团队已满
	IEO_S_JOIN_TEAM_FAILED              uint64 = 9003 //入团失败
	IEO_S_AMOUNT_NOT_ENOUGH             uint64 = 9004 //数量小于最小申购额
	IEO_S_REDEEM_CODE_ACTIVATED_FAILED  uint64 = 9005 //兑换码激活失败
	IEO_S_PURCHASE_FAILED               uint64 = 9006 //申购失败
	IEO_S_OPEN_TEAM_FAILED              uint64 = 9007 //开团失败

	FROZEN_ASSET_TRADE_CANCELED_FAILED uint64 = 9100 //冻结资产撤单失败
	FROZEN_ASSET_TRADE_PREDELIVERY     uint64 = 9101 //交割期禁止下单
	FROZEN_ASSET_TRADE_FEE_NOT_ENOUGH  uint64 = 9102 //手续费不足
	FROZEN_ASSET_TRADE_INVALID_ORDER   uint64 = 9103 //订单失效

	FROZEN_REWARD_OVER_AMOUNT_LIMIT uint64 = 9201 //锁仓有奖份额已满
	FROZEN_REWARD_REDEEM_FAILED     uint64 = 9202 //赎回失败
	FROZEN_REWARD_ACTIVITY_DISABLE  uint64 = 9203 //不在活动进行中

	CONTRACT_ACCOUNT_EXIST             uint64 = 8100 //合约账户已开通
	CONTRACT_ACCOUNT_NOT_EXIST         uint64 = 8101 //合约账户未开通
	CONTRACT_ACCOUNT_OPEN_FAIL         uint64 = 8102 //合约账户开通失败
	CONTRACT_BALANCE_NOT_ENOUGH        uint64 = 8103 //合约账户余额不足
	CONTRACT_TOKEN_TIME_OUT            uint64 = 8104 //合约账户token超时
	CONTRACT_TRANSFER_AMOUNT_ERROR     uint64 = 8105 //合约划转金额错误，小于等于0
	CONTRACT_TRANSFER_DIRECTION_ERROR  uint64 = 8106 // 划转方向错误
	CONTRACT_TRANSFER_Failed           uint64 = 8107 // 划转失败，1分钟后返回账户
	CONTRACT_TRANSFER_TESTTOKEN_Failed uint64 = 8108 // 测试币无法划转

	ALLCOIN_CONTRACT_ACCOUNT_EXIST                   uint64 = 8200 //全币种合约账户已开通
	ALLCOIN_CONTRACT_ACCOUNT_NOT_EXIST               uint64 = 8201 //全币种合约账户未开通
	ALLCOIN_CONTRACT_QUANTITY_ERROR                  uint64 = 8202 //数量错误
	ALLCOIN_CONTRACT_CURRENCY_ERROR                  uint64 = 8203 //币种错误
	ALLCOIN_CONTRACT_LEVERAGE_ERROR                  uint64 = 8204 //杠杆错误
	ALLCOIN_CONTRACT_CURRENCY_TRADE_CLOSE            uint64 = 8205 //币种交易关闭
	ALLCOIN_CONTRACT_CURRENCY_SIDE_CLOSE             uint64 = 8206 //币种某方向交易关闭
	ALLCOIN_CONTRACT_QUANTITY_TOO_SMALL              uint64 = 8207 //数量太小
	ALLCOIN_CONTRACT_QUANTITY_TOO_MUCH               uint64 = 8208 //数量太大
	ALLCOIN_CONTRACT_SIDE_ERROR                      uint64 = 8209 //方向错误
	ALLCOIN_CONTRACT_BALANCE_NOT_ENOUGH              uint64 = 8210 //余额不足
	ALLCOIN_CONTRACT_POSITION_NOT_FOUND              uint64 = 8211 //仓位不存在
	ALLCOIN_CONTRACT_POSITION_CLOSED                 uint64 = 8212 //仓位已平仓
	ALLCOIN_CONTRACT_USER_SIDE_POSITION_TOO_MUCH     uint64 = 8213 //用户单边持仓过大
	ALLCOIN_CONTRACT_MARKET_NET_POSITION_TOO_MUCH    uint64 = 8214 //市场净头寸过大
	ALLCOIN_CONTRACT_PROFIT_PRICE_ERROR              uint64 = 8215 //止盈价错误
	ALLCOIN_CONTRACT_LIQUIDATED_PRICE_ERROR          uint64 = 8216 //止损价错误
	ALLCOIN_CONTRACT_PROFIT_PRICE_OUT_RANGE          uint64 = 8217 //止盈价不在合理范围
	ALLCOIN_CONTRACT_LIQUIDATED_PRICE_OUT_RANGE      uint64 = 8218 //止损价不在合理范围
	ALLCOIN_CONTRACT_PRICE_ERROR                     uint64 = 8219 //委托价格不在合理范围
	ALLCOIN_CONTRACT_USER_SIDE_ORDER_TOO_MUCH        uint64 = 8220 //用户单边委托数量过大
	ALLCOIN_CONTRACT_MARKET_NET_ORDER_TOO_MUCH       uint64 = 8221 //市场单边委托数量过大
	ALLCOIN_CONTRACT_ORDER_NOT_FOUND                 uint64 = 8222 //委托不存在
	ALLCOIN_CONTRACT_ORDER_CANCELLED                 uint64 = 8223 //委托已撤销
	ALLCOIN_CONTRACT_ORDER_SET_PROFIT_AND_LIQUIDATED uint64 = 8224 // 设置止盈止损失败

	MARKET_RATE_NOT_FOUND uint64 = 50001 //取汇率失败

	ACTIVITY_ALREADY_ACQUIRE uint64 = 60001 //已经领取过
	ACTIVITY_HAVE_NO_BALANCE uint64 = 60002 // 无持仓

	FINANCE_API_INTERNAL_ERROR uint64 = 70001 //api 内部错误
)
